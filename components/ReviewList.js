app.component('review-list', {
  props: {
    reviews: {
      type: Array,
      required: true
    }
  },
  template:
  /*html*/
  `<div class="review-container" v-if="reviews.length">
    <h3>Reviews: </h3>
    
    <ul>
      <li v-for="(review, index) in reviews" :key="index">
        >>>>{{ review.name }} gave this {{ review.rating }} stars
        <br>
        <p>{{ review.review }} </p>
        <p v-if="review.recommendation === 'Yes'">{{ review.name }} would like to recommend this product</p>
        <p v-else>{{ review.name }} would not like to recommend this product</p>
      </li>
    </ul>
  </div>`,
})